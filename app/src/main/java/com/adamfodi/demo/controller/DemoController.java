package com.adamfodi.demo.controller;

import com.adamfodi.demo.model.Demo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@RestController
public class DemoController {

    private static final String template = "Hello, %s";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/hello")
    public Demo hello(@RequestParam(value = "name", defaultValue = "World")String name) {
        log.info("hello method called");
        return Demo.builder()
                .id(counter.incrementAndGet())
                .content(String.format(template, name))
                .build();
    }


}
